declare var require: any;
export const environment = {
  production: true,
  service_base_url: 'http://192.168.10.4:6980',
  login_base_url: 'http://192.168.10.4:6969',
  VERSION: require('../../package.json').version
};
