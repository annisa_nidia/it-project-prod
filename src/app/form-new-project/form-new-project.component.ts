import {Component, ElementRef, Inject, OnInit, ViewChild} from '@angular/core';
import {ProjectService} from '../service/project.service';
import {Project} from '../model/project';
import {Observable} from 'rxjs/Rx';
import {APP_CONFIG} from '../model/config';
import {FormBuilder, FormGroup} from "@angular/forms";
import {DatePipe} from "@angular/common";
import {ToastsManager} from "ng2-toastr";

import * as moment from 'moment';
import {Router} from "@angular/router";

declare let _: any;

@Component({
  selector: 'app-form-new-project',
  templateUrl: './form-new-project.component.html',
  styleUrls: ['./form-new-project.component.css'],
  providers: [DatePipe]
})
export class FormNewProjectComponent implements OnInit {

  @ViewChild('projectNameInput')
  projectNameInput: ElementRef;

  projectList: Project[] = [];
  itListAllProject = null;
  itListWaitingProject = null;
  itListCurrentProject = null;
  itListDoneProject = null;
  todaysDate: Date;
  title = 'All Project';
  titleModal: string;
  pageNo = 0;

  doOpenForm = false;
  openFormProduction = false;
  openFormDevelopment = false;
  openFormHold = false;
  openFormContinue = false;
  openFormDelete = false;
  projectFormBuilder: FormGroup;
  developmentFormBuilder: FormGroup;
  selectedPriority: string;
  selectedStatus: string;
  selectedWaitingProject: Project;
  selectedHoldProject: Project;
  selectedContinueProject: Project;
  selectedProductionProject: Project;
  selectedDeleteProject: Project;
  openedForm = 'Waiting';
  selectedId: number;
  selectedEditStatus: string;

  startDate: Date = new Date();
  liveTarget: Date = new Date();

  loading = false;

  constructor(@Inject(APP_CONFIG) private appConfig, private projectService: ProjectService, private router: Router,
              private fb: FormBuilder, private datePipe: DatePipe, private toastr: ToastsManager) {
  }

  ngOnInit(): void {
    this.todaysDate = new Date();
    this.projectFormBuilder = this.fb.group({
      projectName: [''],
      priority: [''],
      requestBy: [''],
      startDate: [new Date()],
      liveTarget: [new Date()],
      division: [''],
      status: [''],
      pic: [''],
      description: []
    });
    this.developmentFormBuilder = this.fb.group({
      priority: [''],
      startDate: [new Date()],
      liveTarget: [new Date()],
      status: [''],
      pic: [''],
      description: []
    });

    this.itListAllProject = {
      'proj': [{
        'projectName': 'Data Warehouse + Tableau',
        'priority': '1',
        'requester': 'Head Office',
        'startDate': '01 Nov 2017',
        'goLiveTarget': '03 Mar 2018',
        'division': 'EDP',
        'currentStatus': 'Development',
        'pic': 'Zulmy,Dyla,Cybertren',
        'description': 'Sprint 3'
      }, {
        'projectName': 'Ekspedisi',
        'priority': '3',
        'requester': 'Logistic Department',
        'startDate': '15 Oct 2017',
        'goLiveTarget': '01 Dec 2017',
        'division': 'Solution',
        'currentStatus': 'Development',
        'pic': 'Arin,Nisa'
      }, {
        'projectName': 'Payment Method',
        'priority': '2',
        'requester': 'Finance Department',
        'startDate': '10 Oct 2017',
        'goLiveTarget': '15 Dec 2017',
        'division': 'Solution',
        'currentStatus': 'Development',
        'pic': 'Henda'
      }, {
        'projectName': 'QR Code BNI',
        'priority': '4',
        'requester': 'Head Office',
        'startDate': '15 Oct 2017',
        'goLiveTarget': '',
        'division': 'Solution',
        'currentStatus': 'Hold',
        'pic': 'Henda'
      }, {
        'projectName': 'Voucher Online',
        'priority': '2',
        'requester': 'Head Office',
        'startDate': '10 Nov 2017',
        'goLiveTarget': '15 Dec 2017',
        'division': 'Solution',
        'currentStatus': 'Development',
        'pic': 'Henda,Nisa'
      }, {
        'projectName': 'Member Loyalty',
        'priority': '2',
        'requester': 'Head Office',
        'startDate': '01 Oct 2017',
        'goLiveTarget': '',
        'division': 'Solution',
        'currentStatus': 'Development',
        'pic': 'Henda'
      }, {
        'projectName': 'Food Bus',
        'priority': '2',
        'requester': 'Head Office',
        'startDate': '01 Nov 2017',
        'goLiveTarget': '01 Dec 2017',
        'division': 'Solution',
        'currentStatus': 'Development',
        'pic': 'Henda,Arin'
      }, {
        'projectName': 'KFC Box',
        'priority': '3',
        'requester': 'Head Office',
        'startDate': '10 Nov 2017',
        'goLiveTarget': '01 Jan 2018',
        'division': 'Solution',
        'currentStatus': 'Development',
        'pic': 'Henda,Pandu'
      }, {
        'projectName': 'MDD',
        'priority': '2',
        'requester': 'Market Development Department',
        'startDate': '02 Jan 2018',
        'goLiveTarget': '15 Jan 2018',
        'division': 'Solution',
        'currentStatus': 'Development',
        'pic': ''
      }, {
        'projectName': 'E-Maintenance phase 2',
        'priority': '2',
        'requester': 'Information Technology Department',
        'startDate': '02 Jan 2018',
        'goLiveTarget': '31 mar 2018',
        'division': 'Solution',
        'currentStatus': 'Development',
        'pic': ''
      }, {
        'projectName': 'Settlement',
        'priority': '2',
        'requester': 'Finance Department',
        'startDate': '8 Nov 2017',
        'goLiveTarget': '20 Nov 2017',
        'division': 'Solution',
        'currentStatus': 'Production',
        'pic': 'Pandu,Henda'
      }, {
        'projectName': 'Table Service',
        'priority': '1',
        'requester': 'Head Office',
        'startDate': '10 Oct 2017',
        'goLiveTarget': '15 Dec 2017',
        'division': 'Solution',
        'currentStatus': 'Production',
        'pic': 'Henda'
      }, {
        'projectName': 'SST',
        'priority': '1',
        'requester': 'Head Office',
        'startDate': '10 Oct 2017',
        'goLiveTarget': '15 Dec 2017',
        'division': 'Solution',
        'currentStatus': 'Production',
        'pic': 'Nisa'
      }, {
        'projectName': 'API Pajak',
        'priority': '3',
        'requester': 'Head Office',
        'startDate': '03 Nov 2017',
        'goLiveTarget': '10 Nov 2017',
        'division': 'Solution',
        'currentStatus': 'Production',
        'pic': 'Henda'
      }, {
        'projectName': 'Report Bandara',
        'priority': '3',
        'requester': 'Head Office',
        'startDate': '03 Nov 2017',
        'goLiveTarget': '10 Nov 2017',
        'division': 'Solution',
        'currentStatus': 'Production',
        'pic': 'Henda,Pandu'
      }, {
        'projectName': 'Travel Desk',
        'priority': '',
        'requester': 'Head Office',
        'startDate': '',
        'goLiveTarget': '',
        'division': 'Solution',
        'currentStatus': 'Waiting',
        'pic': ''
      }, {
        'projectName': 'QR Code Vending',
        'priority': '',
        'requester': 'Head Office',
        'startDate': '',
        'goLiveTarget': '',
        'division': 'Solution',
        'currentStatus': 'Waiting',
        'pic': ''
      }, {
        'projectName': 'NSO Apps',
        'priority': '',
        'requester': 'Head Office',
        'startDate': '',
        'goLiveTarget': '',
        'division': 'Solution',
        'currentStatus': 'Waiting',
        'pic': ''
      }, {
        'projectName': 'Vending Machine',
        'priority': '',
        'requester': 'Head Office',
        'startDate': '',
        'goLiveTarget': '',
        'division': 'Solution',
        'currentStatus': 'Waiting',
        'pic': ''
      }, {
        'projectName': 'Catering Commission',
        'priority': '',
        'requester': 'Business Unit Department',
        'startDate': '',
        'goLiveTarget': '',
        'division': 'Solution',
        'currentStatus': 'Waiting',
        'pic': ''
      }, {
        'projectName': 'IT Asset Management',
        'priority': '',
        'requester': 'Information Technology',
        'startDate': '',
        'goLiveTarget': '',
        'division': 'Solution',
        'currentStatus': 'Waiting',
        'pic': ''
      }]
    };

    this.doGetListWaitingProject();
    this.developmentFormBuilder.get('priority').valueChanges.subscribe(data => this.selectedPriority = data);
    this.projectFormBuilder.get('priority').valueChanges.subscribe(data => this.selectedPriority = data);
    this.developmentFormBuilder.get('status').valueChanges.subscribe(data => this.selectedStatus = data);
    this.projectFormBuilder.get('status').valueChanges.subscribe(data => this.selectedStatus = data);
  }

  doGetListWaitingProject() {
    this.projectService.getListITWaitingProject().subscribe((res) => {
      const p = res.data;
      this.projectList = _.sortBy(p, ['priority', 'division']);
    });
  }

  doGetListCurrentProject() {
    this.projectService.getListITCurrentProject().subscribe((res) => {
      const p = res.data;
      this.projectList = _.sortBy(p, ['priority', 'division']);
    });
  }

  doGetListDoneProject() {
    this.projectService.getListITDoneProject().subscribe((res) => {
      const p = res.data;
      this.projectList = _.sortBy(p, ['priority', 'division']);
    });
  }

  openTabWaiting() {
    this.openedForm = 'Waiting';
    this.doGetListWaitingProject();
  }

  openTabCurrent() {
    this.openedForm = 'Current';
    this.doGetListCurrentProject();
  }

  openTabDone() {
    this.openedForm = 'Finish';
    this.doGetListDoneProject();
  }

  doOpenFormAdd() {
    this.openedForm = 'Waiting';
    this.doOpenForm = true;
    this.doSetFocusProjectName();
    this.titleModal = 'Add New Project';
    this.projectFormBuilder.reset();
  }

  doOpenFormEdit(data: Project) {
    this.doOpenForm = true;
    this.titleModal = 'Edit Project Data';
    this.projectFormBuilder.get('projectName').setValue(data.projectName);
    this.projectFormBuilder.get('requestBy').setValue(data.requester);
    this.projectFormBuilder.get('division').setValue(data.division);
    this.projectFormBuilder.get('priority').setValue(data.priority);
    this.projectFormBuilder.get('startDate').setValue(moment(data.startDate === '1970-01-01 07:00:00.0' ? new Date() : data.startDate).toDate());
    this.projectFormBuilder.get('liveTarget').setValue(moment(data.goLiveTarget === '1970-01-01 07:00:00.0' ? new Date() : data.goLiveTarget).toDate());
    this.projectFormBuilder.get('pic').setValue(data.pic);
    this.projectFormBuilder.get('description').setValue(data.description);
    // this.projectFormBuilder.get('status').setValue(data.currentStatus);
    this.selectedId = data.id;
    this.selectedEditStatus = data.currentStatus;
    this.selectedWaitingProject = data;
  }

  doOpenFormDevelopment(data: Project) {
    this.titleModal = 'Project Development Info (' + data.projectName + ')';
    this.openFormDevelopment = true;
    this.developmentFormBuilder.reset();
    this.selectedWaitingProject = data;
  }

  doOpenFormHold(data: Project) {
    this.openFormHold = true;
    this.selectedHoldProject = data;
  }

  doOpenFormContinue(data: Project) {
    this.openFormContinue = true;
    this.selectedContinueProject = data;
  }

  doOpenFormProduction(data: Project) {
    this.openFormProduction = true;
    this.selectedProductionProject = data;
  }

  doOpenFormDelete(data: Project) {
    this.openFormDelete = true;
    this.selectedDeleteProject = data;
  }

  doSubmit() {
    if (this.titleModal === 'Add New Project') {
      this.doAddProject();
    } else if (this.titleModal === 'Project Development Info (' + this.selectedWaitingProject.projectName + ')') {
      this.doEditProjectDevelopment();
    } else if (this.titleModal === 'Edit Project Data') {
      if (this.openedForm === 'Waiting' && this.selectedEditStatus !== 'HOLD') {
        this.doEditWaitingProject();
      } else if (this.openedForm === 'Waiting' && this.selectedEditStatus === 'HOLD') {
        this.selectedEditStatus = 'HOLD';
        this.doEditProject();
      } else if (this.openedForm === 'Current') {
        this.selectedEditStatus = 'DEVELOPMENT';
        this.doEditProject();
      } else if (this.openedForm === 'Finish') {
        this.selectedEditStatus = 'PRODUCTION';
        this.doEditProject();
      }
    }
  }

  doAddProject() {
    this.loading = true;
    const param: any = {
      projectName: this.projectFormBuilder.get('projectName').value,
      priority: '',
      requester: this.projectFormBuilder.get('requestBy').value,
      startDate: '1970-01-01',
      goLiveTarget: '1970-01-01',
      division: this.projectFormBuilder.get('division').value,
      currentStatus: 'WAITING',
      pic: '',
      description: ''
    };

    this.projectService.doInsertProject(param).subscribe((res) => {
      this.toastr.success('Data have been saved successfully!', 'Success!');
      this.loading = false;
      this.doGetListWaitingProject();
    }, (err) => {
      this.loading = false;
      this.toastr.error('An error occure while saving data!', 'Ooops!');
    });
  }

  doEditWaitingProject() {
    const param: any = {
      projectName: this.projectFormBuilder.get('projectName').value,
      priority: '',
      requester: this.projectFormBuilder.get('requestBy').value,
      startDate: '1970-01-01',
      goLiveTarget: '1970-01-01',
      division: this.projectFormBuilder.get('division').value,
      currentStatus: 'WAITING',
      pic: '',
      description: '',
      id: this.selectedId
    };

    this.projectService.doUpdateProject(param).subscribe((res) => {
      this.toastr.success('Data have been saved successfully!', 'Success!');
      this.loading = false;
      this.doGetListWaitingProject();
    }, (err) => {
      this.loading = false;
      this.toastr.error('An error occure while saving data!', 'Ooops!');
    });
  }

  doEditProject() {
    const param: any = {
      projectName: this.projectFormBuilder.get('projectName').value,
      priority: this.selectedPriority,
      requester: this.projectFormBuilder.get('requestBy').value,
      startDate: this.datePipe.transform(this.startDate, 'yyyy-MM-dd'),
      goLiveTarget: this.datePipe.transform(this.liveTarget, 'yyyy-MM-dd'),
      division: this.projectFormBuilder.get('division').value,
      currentStatus: this.selectedEditStatus,
      pic: this.projectFormBuilder.get('pic').value,
      description: this.projectFormBuilder.get('description').value,
      id: this.selectedId
    };

    this.projectService.doUpdateProject(param).subscribe((res) => {
      this.toastr.success('Data have been saved successfully!', 'Success!');
      this.loading = false;
      if (this.openedForm === 'Waiting') {
        this.doGetListWaitingProject();
      } else if (this.openedForm === 'Current') {
        this.doGetListCurrentProject();
      } else if (this.openedForm === 'Finish') {
        this.doGetListDoneProject();
      }
    }, (err) => {
      this.loading = false;
      this.toastr.error('An error occure while saving data!', 'Ooops!');
    });
  }

  doEditProjectDevelopment() {
    const param: any = {
      projectName: this.selectedWaitingProject.projectName,
      priority: this.selectedPriority,
      requester: this.selectedWaitingProject.requester,
      startDate: this.datePipe.transform(this.startDate, 'yyyy-MM-dd'),
      goLiveTarget: this.datePipe.transform(this.liveTarget, 'yyyy-MM-dd'),
      division: this.selectedWaitingProject.division,
      currentStatus: 'DEVELOPMENT',
      pic: this.developmentFormBuilder.get('pic').value,
      description: this.developmentFormBuilder.get('description').value,
      id: this.selectedWaitingProject.id
    };
    // console.log(param);
    this.projectService.doUpdateProject(param).subscribe((res) => {
      this.toastr.success('Data have been saved successfully!', 'Success!');
      this.loading = false;
      if (this.openedForm === 'Waiting') {
        this.doGetListWaitingProject();
      } else if (this.openedForm === 'Current') {
        this.doGetListCurrentProject();
      } else if (this.openedForm === 'Finish') {
        this.doGetListDoneProject();
      }
    }, (err) => {
      this.loading = false;
      this.toastr.error('An error occure while saving data!', 'Ooops!');
    });
  }

  doHoldProject() {
    const param: any = {
      projectName: this.selectedHoldProject.projectName,
      priority: this.selectedHoldProject.priority,
      requester: this.selectedHoldProject.requester,
      startDate: this.datePipe.transform(this.selectedHoldProject.startDate, 'yyyy-MM-dd'),
      goLiveTarget: this.datePipe.transform(this.selectedHoldProject.goLiveTarget, 'yyyy-MM-dd'),
      division: this.selectedHoldProject.division,
      currentStatus: 'HOLD',
      pic: this.selectedHoldProject.pic,
      description: this.selectedHoldProject.description,
      id: this.selectedHoldProject.id
    };

    this.projectService.doUpdateProject(param).subscribe((res) => {
      this.toastr.success('Data have been saved successfully!', 'Success!');
      this.loading = false;
      if (this.openedForm === 'Waiting') {
        this.doGetListWaitingProject();
      } else if (this.openedForm === 'Current') {
        this.doGetListCurrentProject();
      } else if (this.openedForm === 'Finish') {
        this.doGetListDoneProject();
      }
    }, (err) => {
      this.loading = false;
      this.toastr.error('An error occure while saving data!', 'Ooops!');
    });
  }

  doContinueProject() {
    const param: any = {
      projectName: this.selectedContinueProject.projectName,
      priority: this.selectedContinueProject.priority,
      requester: this.selectedContinueProject.requester,
      startDate: this.datePipe.transform(this.selectedContinueProject.startDate, 'yyyy-MM-dd'),
      goLiveTarget: this.datePipe.transform(this.selectedContinueProject.goLiveTarget, 'yyyy-MM-dd'),
      division: this.selectedContinueProject.division,
      currentStatus: 'DEVELOPMENT',
      pic: this.selectedContinueProject.pic,
      description: this.selectedContinueProject.description,
      id: this.selectedContinueProject.id
    };

    this.projectService.doUpdateProject(param).subscribe((res) => {
      this.toastr.success('Data have been saved successfully!', 'Success!');
      this.loading = false;
      if (this.openedForm === 'Waiting') {
        this.doGetListWaitingProject();
      } else if (this.openedForm === 'Current') {
        this.doGetListCurrentProject();
      } else if (this.openedForm === 'Finish') {
        this.doGetListDoneProject();
      }
    }, (err) => {
      this.loading = false;
      this.toastr.error('An error occure while saving data!', 'Ooops!');
    });
  }

  doFinishProject() {
    const param: any = {
      projectName: this.selectedProductionProject.projectName,
      priority: this.selectedProductionProject.priority,
      requester: this.selectedProductionProject.requester,
      startDate: this.datePipe.transform(this.selectedProductionProject.startDate, 'yyyy-MM-dd'),
      goLiveTarget: this.datePipe.transform(this.selectedProductionProject.goLiveTarget, 'yyyy-MM-dd'),
      division: this.selectedProductionProject.division,
      currentStatus: 'PRODUCTION',
      pic: this.selectedProductionProject.pic,
      description: this.selectedProductionProject.description,
      id: this.selectedProductionProject.id
    };

    this.projectService.doUpdateProject(param).subscribe((res) => {
      this.toastr.success('Data have been saved successfully!', 'Success!');
      this.loading = false;
      if (this.openedForm === 'Waiting') {
        this.doGetListWaitingProject();
      } else if (this.openedForm === 'Current') {
        this.doGetListCurrentProject();
      } else if (this.openedForm === 'Finish') {
        this.doGetListDoneProject();
      }
    }, (err) => {
      this.loading = false;
      this.toastr.error('An error occure while saving data!', 'Ooops!');
    });
  }

  doDeleteProject() {
    const param: any = {
      id: this.selectedDeleteProject.id
    };

    this.projectService.doDeleteProject(param).subscribe((res) => {
      this.toastr.success('Data have been deleted successfully!', 'Success!');
      this.loading = false;
      this.doGetListWaitingProject();
    }, (err) => {
      this.loading = false;
      this.toastr.error('An error occure while saving data!', 'Ooops!');
    });
  }

  get projectName() {
    return this.projectFormBuilder.get('projectName');
  }
  get priority() {
    return this.projectFormBuilder.get('priority');
  }
  get requestBy() {
    return this.projectFormBuilder.get('requestBy');
  }
  get startedDate() {
    return this.projectFormBuilder.get('startDate');
  }
  get liveDate() {
    return this.projectFormBuilder.get('liveTarget');
  }
  get division() {
    return this.projectFormBuilder.get('division');
  }
  get status() {
    return this.projectFormBuilder.get('status');
  }
  get pic() {
    return this.projectFormBuilder.get('pic');
  }
  get description() {
    return this.projectFormBuilder.get('description');
  }

  doLogout() {
    this.router.navigate(['login']);
    localStorage.clear();
  }

  doSetFocusProjectName() {
    Observable.timer(30).do(() => {
      this.projectNameInput.nativeElement.focus();
    }).subscribe();
  }
}
