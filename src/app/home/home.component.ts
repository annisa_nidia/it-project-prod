import {Component, Inject, OnInit, AfterViewInit, ViewChild, ElementRef} from '@angular/core';
import {ProjectService} from '../service/project.service';
import {Project} from '../model/project';
import {APP_CONFIG} from '../model/config';
import {DatePipe} from '@angular/common';
import { interval, fromEvent, merge, empty, timer } from 'rxjs';
import { switchMap, scan, takeWhile, startWith, mapTo } from 'rxjs/operators';

declare let _: any;

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css'],
  providers: [DatePipe]
})
export class HomeComponent implements OnInit, AfterViewInit {

  @ViewChild('pause') pause: ElementRef;
  @ViewChild('resume') resume: ElementRef;

  projectList: Project[] = [];
  itListProject = null;
  itListDoneProject = null;
  itListWaitingProject = null;
  todaysDate: Date;
  val: number;
  showFlag= 1;
  stoppedShowFlag: number;
  title = 'Current Project';
  isPause = false;

  subs: any;
  slideshow: any;
  pageNo = 0;

  constructor(@Inject(APP_CONFIG) private appConfig, private projectService: ProjectService, private datePipe: DatePipe) {
  }

  ngOnInit(): void {
    this.todaysDate = new Date();
    this.itListProject = {
      'proj': [{
        'projectName': 'Data Warehouse + Tableau',
        'priority': '1',
        'requester': 'Head Office',
        'startDate': '01 Nov 2017',
        'goLiveTarget': '03 Mar 2018',
        'division': 'EDP',
        'currentStatus': 'Development',
        'pic': 'Zulmy,Dyla,Cybertren',
        'description': 'Sprint 3'
      }, {
        'projectName': 'Ekspedisi',
        'priority': '3',
        'requester': 'Logistic Department',
        'startDate': '15 Oct 2017',
        'goLiveTarget': '01 Dec 2017',
        'division': 'Solution',
        'currentStatus': 'Development',
        'pic': 'Arin,Nisa'
      }, {
        'projectName': 'Payment Method',
        'priority': '2',
        'requester': 'Finance Department',
        'startDate': '10 Oct 2017',
        'goLiveTarget': '15 Dec 2017',
        'division': 'Solution',
        'currentStatus': 'Development',
        'pic': 'Henda'
      }, {
        'projectName': 'QR Code BNI',
        'priority': '4',
        'requester': 'Head Office',
        'startDate': '15 Oct 2017',
        'goLiveTarget': '',
        'division': 'Solution',
        'currentStatus': 'Hold',
        'pic': 'Henda'
      }, {
        'projectName': 'Voucher Online',
        'priority': '2',
        'requester': 'Head Office',
        'startDate': '10 Nov 2017',
        'goLiveTarget': '15 Dec 2017',
        'division': 'Solution',
        'currentStatus': 'Development',
        'pic': 'Henda,Nisa'
      }, {
        'projectName': 'Member Loyalty',
        'priority': '2',
        'requester': 'Head Office',
        'startDate': '01 Oct 2017',
        'goLiveTarget': '',
        'division': 'Solution',
        'currentStatus': 'Development',
        'pic': 'Henda'
      }, {
        'projectName': 'Food Bus',
        'priority': '2',
        'requester': 'Head Office',
        'startDate': '01 Nov 2017',
        'goLiveTarget': '01 Dec 2017',
        'division': 'Solution',
        'currentStatus': 'Development',
        'pic': 'Henda,Arin'
      }, {
        'projectName': 'KFC Box',
        'priority': '3',
        'requester': 'Head Office',
        'startDate': '10 Nov 2017',
        'goLiveTarget': '01 Jan 2018',
        'division': 'Solution',
        'currentStatus': 'Development',
        'pic': 'Henda,Pandu'
      }, {
        'projectName': 'MDD',
        'priority': '2',
        'requester': 'Market Development Department',
        'startDate': '02 Jan 2018',
        'goLiveTarget': '15 Jan 2018',
        'division': 'Solution',
        'currentStatus': 'Development',
        'pic': ''
      }, {
        'projectName': 'E-Maintenance phase 2',
        'priority': '2',
        'requester': 'Information Technology Department',
        'startDate': '02 Jan 2018',
        'goLiveTarget': '31 mar 2018',
        'division': 'Solution',
        'currentStatus': 'Development',
        'pic': ''
      }]
    };
    this.itListDoneProject = {
      'proj': [{
        'projectName': 'Settlement',
        'priority': '2',
        'requester': 'Finance Department',
        'startDate': '8 Nov 2017',
        'goLiveTarget': '20 Nov 2017',
        'division': 'Solution',
        'currentStatus': 'Production',
        'pic': 'Pandu,Henda'
      }, {
        'projectName': 'Table Service',
        'priority': '1',
        'requester': 'Head Office',
        'startDate': '10 Oct 2017',
        'goLiveTarget': '15 Dec 2017',
        'division': 'Solution',
        'currentStatus': 'Production',
        'pic': 'Henda'
      }, {
        'projectName': 'SST',
        'priority': '1',
        'requester': 'Head Office',
        'startDate': '10 Oct 2017',
        'goLiveTarget': '15 Dec 2017',
        'division': 'Solution',
        'currentStatus': 'Production',
        'pic': 'Nisa'
      }, {
        'projectName': 'API Pajak',
        'priority': '3',
        'requester': 'Head Office',
        'startDate': '03 Nov 2017',
        'goLiveTarget': '10 Nov 2017',
        'division': 'Solution',
        'currentStatus': 'Production',
        'pic': 'Henda'
      }, {
        'projectName': 'Report Bandara',
        'priority': '3',
        'requester': 'Head Office',
        'startDate': '03 Nov 2017',
        'goLiveTarget': '10 Nov 2017',
        'division': 'Solution',
        'currentStatus': 'Production',
        'pic': 'Henda,Pandu'
      }]
    };
    this.itListWaitingProject = {
      'proj': [{
        'projectName': 'Travel Desk',
        'priority': '',
        'requester': 'Head Office',
        'startDate': '',
        'goLiveTarget': '',
        'division': 'Solution',
        'currentStatus': 'Waiting',
        'pic': ''
      }, {
        'projectName': 'QR Code Vending',
        'priority': '',
        'requester': 'Head Office',
        'startDate': '',
        'goLiveTarget': '',
        'division': 'Solution',
        'currentStatus': 'Waiting',
        'pic': ''
      }, {
        'projectName': 'NSO Apps',
        'priority': '',
        'requester': 'Head Office',
        'startDate': '',
        'goLiveTarget': '',
        'division': 'Solution',
        'currentStatus': 'Waiting',
        'pic': ''
      }, {
        'projectName': 'Vending Machine',
        'priority': '',
        'requester': 'Head Office',
        'startDate': '',
        'goLiveTarget': '',
        'division': 'Solution',
        'currentStatus': 'Waiting',
        'pic': ''
      }, {
        'projectName': 'Catering Commission',
        'priority': '',
        'requester': 'Business Unit Department',
        'startDate': '',
        'goLiveTarget': '',
        'division': 'Solution',
        'currentStatus': 'Waiting',
        'pic': ''
      }, {
        'projectName': 'IT Asset Management',
        'priority': '',
        'requester': 'Information Technology',
        'startDate': '',
        'goLiveTarget': '',
        'division': 'Solution',
        'currentStatus': 'Waiting',
        'pic': ''
      }]
    };


    // this.slideshow = Observable.timer(1000, this.appConfig.changePageTime).subscribe(() => {
    //   this.deactivateTime();
    //   this.activateTime();
    //   if (this.showFlag < 100) {
    //     this.doGetListProject();
    //   } else if (this.showFlag < 200) {
    //     this.doGetWaitingProject();
    //   } else {
    //     this.doGetDoneProject();
    //   }
    // });
    this.doGetListProject();
  }

  ngAfterViewInit() {
    const countdownSeconds = this.appConfig.rundownCounter;
    const interval$ = interval(1000).pipe(mapTo(-1));
    document.getElementById('remaining').innerHTML = countdownSeconds;
    const pause$ = fromEvent(this.pause.nativeElement, 'click').pipe(mapTo(false));
    const resume$ = fromEvent(this.resume.nativeElement, 'click').pipe(mapTo(true));

    const timer$ = merge(pause$, resume$)
      .pipe(
        startWith(true),
        switchMap(val => (val ? interval$ : empty())),
        scan((acc, curr) => (curr ? curr + acc : acc), countdownSeconds),
        takeWhile(v => v >= 0)
      )
      .subscribe(val => {
        document.getElementById('remaining').innerHTML = val;
        if (val === 0) {
          document.getElementById('remaining').innerHTML = countdownSeconds;
          this.changeSlide();
        }
      });

  }

  changeSlide() {
    if (this.showFlag < 100) {
      this.doGetListProject();
    } else if (this.showFlag < 200) {
      this.doGetWaitingProject();
    } else {
      this.doGetDoneProject();
    }

    const countdownSeconds = this.appConfig.rundownCounter;
    const interval$ = interval(1000).pipe(mapTo(-1));
    const setHTML = id => val => (document.getElementById(id).innerHTML = val);
    const pause$ = fromEvent(this.pause.nativeElement, 'click').pipe(mapTo(false));
    const resume$ = fromEvent(this.resume.nativeElement, 'click').pipe(mapTo(true));

    const timer$ = merge(pause$, resume$)
      .pipe(
        startWith(true),
        switchMap(val => (val ? interval$ : empty())),
        scan((acc, curr) => (curr ? curr + acc : acc), countdownSeconds),
        takeWhile(v => v >= 0)
      )
      .subscribe(val => {
        document.getElementById('remaining').innerHTML = val;
        if (val === 0) {
          document.getElementById('remaining').innerHTML = countdownSeconds;
          this.changeSlide();
        }
      });
  }

  doGetListProject() {
    const start: number = ((this.showFlag - 1) * 10) + 1;
    const end: number = this.showFlag * 10;
    this.projectService.getListProject(this.showFlag).subscribe((res) => {
      if (this.showFlag === 1) {
        this.pageNo = 0;
        this.title = 'Current Project';
      } else if (this.showFlag > 1 && this.showFlag < 100) {
        this.title = 'Current Project (Continued)';
        this.pageNo = this.pageNo + 1;
      }
      this.val = this.appConfig.rundownCounter;
      let p = res.data;
      // let p = this.itListProject.proj;
      this.projectList = _.sortBy(p, ['priority', 'division']);
      if (this.projectList && this.projectList.length > 0) {
        this.showFlag = this.showFlag + 1;
      } else {
        this.showFlag = 101;
        this.doGetWaitingProject();
      }
    });
  }

  doGetWaitingProject() {
    const start: number = ((this.showFlag - 100 - 1) * 10) + 1;
    const end: number = (this.showFlag - 100) * 10;
    this.projectService.getListWaitingProject(this.showFlag - 100).subscribe((res) => {
      if (this.showFlag === 101) {
        this.pageNo = 0;
        this.title = 'Waiting Project';
      } else if (this.showFlag > 100 && this.showFlag < 200) {
        this.title = 'Waiting Project (Continued)';
        this.pageNo = this.pageNo + 1;
      }
      this.val = this.appConfig.rundownCounter;
      let p = res.data;
      // let p = this.itListWaitingProject.proj;
      this.projectList = _.sortBy(p, ['priority', 'division']);
      if (this.projectList && this.projectList.length > 0) {
        this.showFlag = this.showFlag + 1;
      } else {
        this.showFlag = 201;
        this.doGetDoneProject();
      }
    });
  }

  doGetDoneProject() {
    const start: number = ((this.showFlag - 200 - 1) * 10) + 1;
    const end: number = (this.showFlag - 200) * 10;

    this.projectService.getListDoneProject(this.showFlag - 200).subscribe((res) => {
      if (this.showFlag === 201) {
        this.pageNo = 0;
        this.title = 'Finish Project';
      } else if (this.showFlag > 200 && this.showFlag < 300) {
        this.title = 'Finish Project (Continued)';
        this.pageNo = this.pageNo + 1;
      }
      this.val = this.appConfig.rundownCounter;
      let p = res.data;
      // let p = this.itListDoneProject.proj;
      this.projectList = _.sortBy(p, ['priority', 'division']);
      if (this.projectList && this.projectList.length > 0) {
        this.showFlag = this.showFlag + 1;
      } else {
        this.showFlag = 1;
        this.doGetListProject();
      }

    });
  }

  activateTime() {
    // this.subs = Observable.timer(1000, 1000).subscribe(() => {
    //   this.val = this.val - 1;
    // });
  }

  deactivateTime() {
    if (this.subs) {
      this.subs.unsubscribe();
    }
  }

  pausedSlideShow() {
    this.isPause = true;
  }

  playSlideShow() {
    this.isPause = false;
  }
}
