import {Component, OnInit, ViewChild, ElementRef} from '@angular/core';
import {Router} from '@angular/router';
import {User} from '../model/user';
import {HttpHeaders } from '@angular/common/http';
import {timer} from 'rxjs/';
import {ProjectService} from '../service/project.service';
import {environment} from "../../environments/environment.prod";

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {
  @ViewChild('userNameInput')
  userNameInput: ElementRef;

  @ViewChild('userPasswordInput')
  userPasswordInput: ElementRef;

  un: string;
  pw: string;
  token: string;
  isErrorFound = false;
  today: number = Date.now();
  loading = false;
  errorMessage;

  version: string = environment.VERSION;

  constructor(private router: Router, private projectService: ProjectService) {}

  ngOnInit() {
    this.doSetFocusUserName();
  }

  doSetFocusUserName() {
    timer(30).do(() => {
      this.userNameInput.nativeElement.focus();
    }).subscribe();
  }

  doSetFocusUserPasswd() {
    timer(30).do(() => {
      this.userPasswordInput.nativeElement.focus();
    }).subscribe();
  }

  doLogin() {
    if (this.un && this.un.length > 0 && this.pw && this.pw.length > 0) {
      this.loading = true;

      const userDetail: User = new User();
      userDetail.username = this.un;
      userDetail.password = this.pw;
      userDetail.access = 'ITPROJ';
      this.projectService.getToken(userDetail)
        .subscribe((res) => {
          localStorage.setItem('token', res.item[0]);
          if (res.success) {
            this.loading = false;
            let headers = new HttpHeaders();
            headers = headers.set('Content-Type', 'application/json; charset=utf-8');
            headers = headers.set('x-auth-token', localStorage.getItem('token'));
            this.projectService.getUserInfo(userDetail, headers)
              .subscribe((resp) => {
                localStorage.setItem('create', resp.data.access[0].create);
                localStorage.setItem('view', resp.data.access[0].view);
                localStorage.setItem('update', resp.data.access[0].update);
                localStorage.setItem('delete', resp.data.access[0].delete);
                localStorage.setItem('refCode', resp.data.access[0].refCode);
                const usr: string = JSON.stringify(resp.data.user);
                localStorage.setItem('user', usr);
                this.router.navigate(['/admin']);

              }, (err) => {
                console.log(err);
              });
          } else {
            this.un = '';
            this.pw = '';
            this.errorMessage = 'Wrong Username or Password';
            this.isErrorFound = true;
            this.loading = false;
          }
        }, (err) => {
          this.errorMessage = 'Connection failed. Please try again';
          this.isErrorFound = true;
          console.log(err);
          this.loading = false;
        });
      this.router.navigate(['/admin']);
    } else {
      this.errorMessage = 'Wrong Username or Password';
      this.isErrorFound = true;
      this.router.navigate(['/login']);
    }
  }
}
