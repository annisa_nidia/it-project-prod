import {InjectionToken} from "@angular/core";

export const APP_CONFIG = new InjectionToken('app.config');
export const appConfig = {
  rundownCounter: 20,
  changePageTime: 20000
}
