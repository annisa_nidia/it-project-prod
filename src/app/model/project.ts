export class Project {
  projectName:string;
  priority:string;
  goLiveTarget:string;
  division:string;
  currentStatus:string;
  pic:string;
  startDate:string;
  requester:string;
  description:string;
  id: number;
}
