import {Injectable} from "@angular/core";
import {Observable} from "rxjs/Rx";
import {HttpClient} from "@angular/common/http";
import {environment} from "../../environments/environment";
import {User} from "../model/user";

@Injectable()
export class ProjectService {

  constructor(private httpClient: HttpClient) {
  }

  getToken(userDetail: User): Observable<any> {
    return this.httpClient.post(environment.login_base_url + '/login', userDetail);
  }

  getUserInfo(userDetail: User, header): Observable<any> {
    return this.httpClient.post(environment.login_base_url + '/role.action', userDetail, {headers: header});
  }

  getListProject(paging: number): Observable<any> {
    // return this.httpClient.get(environment.service_base_url + '/it-proj-monitor/itListProject?page=' + paging);
    return this.httpClient.get(environment.service_base_url + '/itListProject?page=' + paging);
  }

  getListWaitingProject(paging: number): Observable<any> {
    // return this.httpClient.get(environment.service_base_url + '/it-proj-monitor/itListWaitingProject?page=' + paging);
    return this.httpClient.get(environment.service_base_url + '/itListWaitingProject?page=' + paging);
  }

  getListDoneProject(paging: number): Observable<any> {
    // return this.httpClient.get(environment.service_base_url + '/it-proj-monitor/itListWaitingProject?page=' + paging);
    return this.httpClient.get(environment.service_base_url + '/itListDoneProject?page=' + paging);
  }

  getListITDoneProject(): Observable<any> {
    // return this.httpClient.get(environment.service_base_url + '/it-proj-monitor/itListDoneProject?page=' + paging);
    return this.httpClient.get(environment.service_base_url + '/listAllProjectProduction');
  }

  getListITCurrentProject(): Observable<any> {
    // return this.httpClient.get(environment.service_base_url + '/it-proj-monitor/itListProject?page=' + paging);
    return this.httpClient.get(environment.service_base_url + '/listAllProjectDevelopment');
  }

  getListITWaitingProject(): Observable<any> {
    // return this.httpClient.get(environment.service_base_url + '/it-proj-monitor/itListWaitingProject?page=' + paging);
    return this.httpClient.get(environment.service_base_url + '/listAllProjectWaiting');
  }

  getListAllProject(paging: number): Observable<any> {
    // return this.httpClient.get(environment.service_base_url + '/it-proj-monitor/itListDoneProject?page=' + paging);
    return this.httpClient.get(environment.service_base_url + '/itListAllProject?page=' + paging);
  }

  doInsertProject(param: any): Observable<any> {
    return this.httpClient.post(environment.service_base_url + '/save.action', param);
  }

  doUpdateProject(param: any): Observable<any> {
    return this.httpClient.post(environment.service_base_url + '/update.action', param);
  }

  doDeleteProject(param: any): Observable<any> {
    return this.httpClient.post(environment.service_base_url + '/delete.action', param);
  }


}
