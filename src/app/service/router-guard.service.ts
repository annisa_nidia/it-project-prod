import { Injectable} from '@angular/core';
import { CanActivate, Router } from '@angular/router';

@Injectable()
export class RouterGuardService implements CanActivate {

  constructor(private router: Router) {
  }

  canActivate(): boolean {
    if (localStorage.getItem('view') === 'Y') {
      return true;
    }
    this.router.navigate(['/login']);
    return false;
  }
}
