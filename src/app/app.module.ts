import {BrowserModule} from "@angular/platform-browser";
import {NgModule} from "@angular/core";
import {AppComponent} from "./app.component"
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import {ProjectService} from "./service/project.service";
import {HttpClientModule} from "@angular/common/http";
import { HourGlassComponent } from './shared/hour-glass/hour-glass.component';
import {APP_CONFIG, appConfig} from "./model/config";
import { FormNewProjectComponent } from './form-new-project/form-new-project.component';
import {RouterModule, Routes} from "@angular/router";
import {RouterGuardService} from "./service/router-guard.service";
import { HomeComponent } from './home/home.component';
import { BsDatepickerModule } from "ngx-bootstrap";
import {ToastModule, ToastOptions} from "ng2-toastr";
import { CustomOption } from './shared/custom-toast';
import { LoaderComponent } from "./shared/loader/loader.component";
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { LoginComponent } from "./login/login.component";


const appRoutes: Routes = [
  {path: '', component: HomeComponent, pathMatch: 'full'},
  {
    path: 'admin', component: FormNewProjectComponent, pathMatch: 'full', canActivate: [RouterGuardService], children: [
    // path: 'admin', component: FormNewProjectComponent, children: [
      {path: '', component: FormNewProjectComponent, pathMatch: 'full'},
  ]
  },
  {path: 'login', component: LoginComponent, pathMatch: 'full'}
];

@NgModule({
  declarations: [
    AppComponent,
    HourGlassComponent,
    FormNewProjectComponent,
    HomeComponent,
    LoaderComponent,
    LoginComponent
  ],
  imports: [
    BrowserModule,
    HttpClientModule,
    RouterModule,
    RouterModule.forRoot(
      appRoutes,
      {useHash: true}
    ),
    FormsModule,
    BsDatepickerModule.forRoot(),
    ReactiveFormsModule,
    ToastModule.forRoot(),
    BrowserAnimationsModule
  ],
  providers: [
    ProjectService,
    {provide: APP_CONFIG, useValue: appConfig},
    {provide: ToastOptions, useClass: CustomOption},
    RouterGuardService
  ],
  bootstrap: [AppComponent]
})
export class AppModule {
}
