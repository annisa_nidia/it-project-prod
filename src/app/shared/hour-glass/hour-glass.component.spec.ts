import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { HourGlassComponent } from './hour-glass.component';

describe('HourGlassComponent', () => {
  let component: HourGlassComponent;
  let fixture: ComponentFixture<HourGlassComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ HourGlassComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(HourGlassComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
