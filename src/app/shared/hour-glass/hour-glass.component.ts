import {Component, OnInit} from "@angular/core";
import {Observable} from "rxjs/Rx";

@Component({
  selector: 'app-hour-glass',
  templateUrl: './hour-glass.component.html',
  styleUrls: ['./hour-glass.component.css']
})
export class HourGlassComponent implements OnInit {

  hour:string = 'fa-hourglass-start';

  constructor() {
  }

  ngOnInit() {
    Observable.timer(300, 1000).subscribe(()=> {
      if (this.hour === 'fa-hourglass-start') {
        this.hour = 'fa-hourglass-half';
      } else if (this.hour === 'fa-hourglass-half') {
        this.hour = 'fa-hourglass-end';
      } else {
        this.hour = 'fa-hourglass-start';
      }
    });
  }

}
